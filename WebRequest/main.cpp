#include "SFML\Network.hpp"
#include "json.hpp"
#include <iostream>

int main()
{
	sf::Http http;
	http.setHost("http://query.yahooapis.com");
	std::string ciudad;
	std::cin >> ciudad;
	sf::Http::Request request;															
	request.setUri("/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + ciudad + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

	sf::Http::Response response = http.sendRequest(request);

	nlohmann::json jdata = nlohmann::json::parse(response.getBody().c_str());

	std::cout << jdata["query"]["results"]["channel"]["item"]["condition"]["text"] << std::endl;

	std::cin.clear();
	std::cin.ignore(INT_MAX, '\n');
	std::cin.get();

	return 0;
}